
#include <stdbool.h>
#include "Arduino.h"
#include "button.h"
#include "stimer.h"

static ButtonEvent state[NUM_BTN];
uint8_t btn_arr[NUM_BTN] = {BTN_SPEED, BTN_SWING, BTN_FLASH};
static bool buttonRead(uint8_t pin)
{
  return digitalRead(pin);
}

void buttonInit(ButtonEvent initialEvent)
{
  pinMode(BTN_SPEED,INPUT_PULLUP);
  pinMode(BTN_SWING,INPUT_PULLUP);
  pinMode(BTN_FLASH,INPUT_PULLUP);
  for (int i=0; i < NUM_BTN; i++)
    state[i] = initialEvent;
  startTimer(10,buttonProcess,NULL);
}

void buttonProcess(void *data)
{
  static uint32_t lastTick[NUM_BTN];
  static uint32_t pressedTick[NUM_BTN];
  static bool pressed[NUM_BTN];
  startTimer(10,buttonProcess,NULL);
  for (int i = 0; i<NUM_BTN; i++){
    uint8_t pin = btn_arr[i];
    if (lastTick[i] != millis())
    {
      lastTick[i] = millis();
      if (pressed[i] == false && buttonRead(pin) == BUTTON_PRESSED)
      {
        pressed[i] = true;
        // Serial.println("Press");
        pressedTick[i] = millis();
      }
      else if (pressed[i] == true && buttonRead(pin) == BUTTON_RELEASED)
      {
        pressed[i] = false;
        if ((millis() - pressedTick[i]) < BUTTON_LONGPRESS_TICK)
        {
          state[i] = buttonShortPress;
        }
        else
        {
          state[i] = buttonLongPress;
        }
      }
    }
  }
}

ButtonEvent buttonGetState(uint8_t pin)
{
  ButtonEvent currentState = buttonIdle;
  if (pin == BTN_SPEED) {
    currentState = state[0];
    state[0] = buttonIdle;
  }
  if (pin == BTN_SWING) {
    currentState = state[1];
    state[1] = buttonIdle;
  }
  if (pin == BTN_FLASH) {
    currentState = state[2];
    state[2] = buttonIdle;
  }
  return currentState;
}


