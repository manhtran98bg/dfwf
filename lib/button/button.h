
#ifndef __BUTTON_H__
#define __BUTTON_H__

#define BUTTON_PRESSED 0UL
#define BUTTON_RELEASED 1UL

#define BUTTON_LONGPRESS_TICK 1000
#define BUTTON_SHORTPRESS_TICK  100

#define NUM_BTN   3
#define BTN_SPEED D6
#define BTN_SWING D7
#define BTN_FLASH D3
typedef enum {buttonIdle=0, buttonShortPress, buttonLongPress} ButtonEvent;

void buttonInit(ButtonEvent initialEvent);

void buttonProcess(void *data);

ButtonEvent buttonGetState(uint8_t pin);

#endif //__BUTTON_H__
