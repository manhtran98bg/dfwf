typedef enum{
    OFF,
    RHYTHM_MODE,
    SLEEP_MODE,
}mode;
typedef struct 
{
    uint32_t chip_id;
    char id[32];
    String ssid;
    char client_id[32];
    char method[32];
    char tc[32];
    uint32_t ts;
}device_t;

typedef struct {
    uint8_t mode;
    bool power;
    uint8_t fan_speed;
    bool swing;
}fan_t;

