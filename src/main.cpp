#include <Arduino.h>
#include <ESP8266WiFi.h>
#include "Ticker.h"
#include "main.h"
#include "cert.h"
#include <PubSubClient.h>
#include <WiFiClientSecure.h>
#include <PCF8591.h>
#include <Wire.h>
#include <ArduinoJson.h>
#include <string.h>
#include <stdlib.h>
#include "button.h"
#include "stimer.h"
#define LED_PIN 2
#define SWING_OUT D5
#define DELAY_INTERVAL  1 //Delay 2ms between 2 value
#define SLEEP_MODE_DURATION 10000   //300s = 5min
#define DEVICE_ID 4065395176
// #define DEBUG_HEAP

#if (DEVICE_ID == 4065395176)
const char MQTT_USER[] = "4065395176";
const char MQTT_PASS[] = "c3827084e187acf015531af48b185f04";
#elif (DEVICE_ID == 3044755883)
const char MQTT_USER[] = "3044755883";
const char MQTT_PASS[] = "512fc07419cf0296262be5d04d8e4be8";
#endif

char request_topic[64]={0};
char response_topic[64]={0};
void smart_config_start(void);
void smart_config_stop(void);
void smart_config_task(void *param);
static void button_handle(void );
static void set_mode_handle(void);
void mqtt_connect(void);

void fan_rhythm_mode(void);
void fan_sleep_mode(void);
void fan_cancel_mode(void);
void fan_set_speed(fan_t *fan, uint8_t speed);

bool in_smartconfig = 0;

BearSSL::WiFiClientSecure esp_client;
PubSubClient mqtt_client(esp_client);
device_t device;
fan_t fan;
PCF8591 pcf8591;
void wifi_init(void){
  WiFi.mode(WIFI_STA);
  WiFi.setAutoReconnect(true);
  WiFi.persistent(true);
  char wifi_hostname[16]={0};
  sprintf(wifi_hostname,"DFWF.%u",device.chip_id);
  WiFi.setHostname(wifi_hostname);
  WiFi.begin();
}
static void update_status(void){
  DynamicJsonDocument txjson(256);
  char txjson_buffer[255] ={0};
  txjson["id"] = device.id;
  txjson["success"] = true;
  txjson["method"] = "status";
  txjson["values"]["signal"] = -50;
  txjson["values"]["switch"] = fan.power;
  if (fan.mode == OFF) txjson["values"]["mode"] = "off";
  else if (fan.mode == RHYTHM_MODE) txjson["values"]["mode"] = "rhythm";
  else if (fan.mode == SLEEP_MODE) txjson["values"]["mode"] = "sleep";
  txjson["values"]["fan_speed"] = String(fan.fan_speed);
  txjson["values"]["count_down_set"] = "cancel";
  txjson["values"]["swing"] = fan.swing;
  serializeJson(txjson,txjson_buffer,256);
  delay(100);
  mqtt_client.publish(response_topic,txjson_buffer);
  Serial.print("TX JSON ==> ");
  Serial.println(txjson_buffer);
  txjson.clear();
}
void receivedCallback(char* topic, byte* payload, unsigned int length) {
  DynamicJsonDocument txjson(256);
  DynamicJsonDocument rxjson(256);
  char txjson_buffer[255] ={0};
  char payload_buffer[255]= {0};
  memcpy(payload_buffer,payload,length);
  Serial.print("RX JSON ==> ");
  Serial.println(payload_buffer);
  
  DeserializationError err = deserializeJson(rxjson,payload_buffer);
  if (err){
    Serial.print("Error");
    Serial.println(err.c_str());
    return;
  }
  if (!rxjson["id"].isNull()){
    device.chip_id = rxjson["id"].as<uint32_t>();
    const char* id = rxjson["id"];
    memset(device.id,0,32);
    memcpy(device.id,id,strlen(id));
  }
  if (!rxjson["method"].isNull()){
    const char *method = rxjson["method"];
    memset(device.method,0,32);
    memcpy(device.method,method,strlen(method));
  }
  if (!rxjson["tc"].isNull()){
    const char *tc = rxjson["tc"];
    memset(device.tc,0,32);
    memcpy(device.tc,tc,strlen(tc));
  }
  if (!rxjson["ts"].isNull()){
    device.ts = rxjson["ts"].as<uint32_t>();
  }
  txjson["id"] = device.id;
  txjson["tc"] = device.tc;
  txjson["ts"] = device.ts + 1;
  txjson["success"] = true;
  if (!strcmp(device.method,"command")){
    txjson["method"] = device.method;
    if (!rxjson["values"]["mode"].isNull()){
      const char *mode_tmp = rxjson["values"]["mode"].as<const char*>();
      if (!strcmp(mode_tmp,"sleep")) fan.mode = SLEEP_MODE;
      else if (!strcmp(mode_tmp,"rhythm")) fan.mode = RHYTHM_MODE;
      else if (!strcmp(mode_tmp,"off")) fan.mode = OFF;
      txjson["values"]["mode"] = mode_tmp;
    }
    if (!rxjson["values"]["switch"].isNull()){
      bool switch_tmp = rxjson["values"]["switch"].as<bool>();
      if (switch_tmp == true) {
        txjson["values"]["switch"] = switch_tmp;
        txjson["values"]["fan_speed"] = "1";
        fan_set_speed(&fan,1);
      }
      else {
        txjson["values"]["switch"] = switch_tmp;
        fan_set_speed(&fan,0);
        fan.mode = OFF;
      }
      fan.power = switch_tmp;
    }
    if(!rxjson["values"]["fan_speed"].isNull()){
      uint8_t speed_tmp = rxjson["values"]["fan_speed"].as<int>();
      if (speed_tmp > 3) speed_tmp = 3;
      fan_set_speed(&fan,speed_tmp);
      fan.fan_speed = speed_tmp;
      txjson["values"]["fan_speed"] = String(speed_tmp);
    }
    if(!rxjson["values"]["count_down_set"].isNull()){
      Serial.println("count_down_set");
    }
    if(!rxjson["values"]["swing"].isNull()){
      bool swing = rxjson["values"]["swing"].as<bool>();
      fan.swing = swing;
      txjson["values"]["swing"] = swing;
      if (swing) {
        Serial.println("Swing ON");
        digitalWrite(SWING_OUT,HIGH);
      }
      else {
        Serial.println("Swing OFF");
        digitalWrite(SWING_OUT,LOW);
      }
    }
  }
  if (!strcmp(device.method,"get_status")){
    txjson["method"] = "status";
    txjson["values"]["signal"] = -50;
    txjson["values"]["switch"] = fan.power;
    if (fan.mode == OFF) txjson["values"]["mode"] = "off";
    else if (fan.mode == RHYTHM_MODE) txjson["values"]["mode"] = "rhythm";
    else if (fan.mode == SLEEP_MODE) txjson["values"]["mode"] = "sleep";
    txjson["values"]["fan_speed"] = String(fan.fan_speed);
    txjson["values"]["count_down_set"] = "cancel";
    txjson["values"]["swing"] = fan.swing;
  }
  serializeJson(txjson,txjson_buffer,256);
  delay(100);
  mqtt_client.publish(response_topic,txjson_buffer);
  Serial.print("TX JSON ==> ");
  Serial.println(txjson_buffer);
  rxjson.clear();
  txjson.clear();
  return;
}
void mqtt_connect(void){
  Serial.print("Connecting to broker: ");
  if (mqtt_client.connect(device.client_id, MQTT_USER, MQTT_PASS)) {
    Serial.println("connected.");
    if(mqtt_client.subscribe(request_topic) == true) {
      Serial.print("Subcribed topic:");
      Serial.println(request_topic);
    }
  } else {
    Serial.print("failed, status code =");
    Serial.println(mqtt_client.state());
  }
}
void pcf8591_init(void){
  Wire.begin(D1,D2);
  Wire.beginTransmission(0x48);
  uint8_t error = Wire.endTransmission();
  if (error != 0) Serial.println("No PCF8591 on I2C bus");
  else Serial.println("Found PCF8591 on I2C bus");
  pcf8591.begin();
  pcf8591.enableDAC();
}
void fan_set_value(uint8_t val){
  pcf8591.analogWrite(val);
}
/** Set speed = 1, 2, 3 
 *  Speed = 1 <=> val = 77
 *  Speed = 2 <=> val = 166
 *  Speed = 3 <=> val = 255
*/
void fan_set_speed(fan_t *fan, uint8_t speed){
  uint8_t val_arr[4]={0, 77, 166, 255};
  uint8_t old_val = val_arr[fan->fan_speed];
  uint8_t new_val = val_arr[speed];
  int i;
  Serial.println("Change Speed = "+String(fan->fan_speed)+" to Speed = "+String(speed));
  Serial.println(old_val);
  Serial.println(new_val);
  if (old_val >= new_val) {
    for (i = old_val; i >= new_val; i--) {
      fan_set_value(i);
      delay(DELAY_INTERVAL);
    }
  }
  else if (old_val < new_val){
    for (i = old_val; i <= new_val; i++) {
      fan_set_value(i);
      delay(DELAY_INTERVAL);
    }
  }
  fan->fan_speed = speed;
  return;
}
static void button_handle(void){
  ButtonEvent speedState = buttonGetState(BTN_SPEED);
  ButtonEvent swingState = buttonGetState(BTN_SWING);
  ButtonEvent flashState = buttonGetState(BTN_FLASH);
  if (speedState == buttonShortPress){
    uint8_t current_speed = fan.fan_speed;
    if (current_speed == 3) {
      current_speed = 0;  //Turn off Fan
      fan.power = false;
      fan_set_speed(&fan, current_speed);
    }
    else {
      fan.power = true;
      current_speed++;
      fan_set_speed(&fan, current_speed);
    }
    update_status();
  } 

  if (swingState == buttonShortPress){  
    bool swing_state = digitalRead(SWING_OUT);
    if (swing_state) {
      Serial.println("Swing OFF");
      digitalWrite(SWING_OUT,LOW);
      fan.swing = false;
    }
    else {
      Serial.println("Swing ON");
      digitalWrite(SWING_OUT,HIGH);
      fan.swing = true;
    }
    update_status();
  } 

  if (flashState == buttonLongPress) smart_config_start();
}
#ifdef DEBUG_HEAP
static void print_free_heap_task(void *param){
  Serial.println("Free Head size = " + String(ESP.getFreeHeap()));
  startTimer(1000,print_free_heap_task, NULL);
}
#endif
static void set_mode_handle(void){
  if (fan.power){
    if (fan.mode == RHYTHM_MODE) fan_rhythm_mode();
    else if (fan.mode == SLEEP_MODE) fan_sleep_mode();
  }
}
static void mqtt_loop(void *param){
  if (WiFi.status() == WL_CONNECTED){
    if (!mqtt_client.connected()) mqtt_connect();
    else mqtt_client.loop();
  };
  startTimer(10,mqtt_loop,NULL);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pcf8591_init();
  device.chip_id = DEVICE_ID;
  sprintf(device.client_id,"%u",device.chip_id);
  pinMode(LED_PIN,OUTPUT);
  pinMode(SWING_OUT, OUTPUT);
  digitalWrite(LED_PIN,HIGH);
  digitalWrite(SWING_OUT,LOW);
  uint32_t timeout = millis() + 10000;
  wifi_init();
  while(WiFi.status() != WL_CONNECTED){
    delay(100);
    if (millis() > timeout) break;
  }
  if(WiFi.status() == WL_CONNECTED) {
    Serial.println("Wifi Connected to [" + WiFi.SSID() + "]");
    Serial.println("Local IP:" + WiFi.localIP().toString());
  }
  else smart_config_start();
  Serial.println("Device ID:" + String(device.chip_id));
  sprintf(request_topic,"v1/iot/devices/%u/request",device.chip_id);
  sprintf(response_topic,"v1/iot/devices/%u/response",device.chip_id);
  BearSSL::X509List cert(digicert);
  esp_client.setTrustAnchors(&cert);
  esp_client.setInsecure();
  mqtt_client.setServer("digo-vinawind-mqtt.digotech.net",4004);
  mqtt_client.setCallback(receivedCallback);
  mqtt_connect();
  startTimer(10,mqtt_loop,NULL);
  #ifdef DEBUG_HEAP
  startTimer(1000,print_free_heap_task, NULL);
  #endif
  buttonInit(buttonIdle);
}

void loop() {
  processTimerEvents();
  button_handle();
  set_mode_handle();
}
void fan_rhythm_mode(void){
  uint8_t cur_speed = 1;
  Serial.println("Enter Rhythm mode");
  fan_set_speed(&fan, cur_speed);
  update_status();
  uint32_t interval = millis();
  while (fan.mode == RHYTHM_MODE){
    processTimerEvents();
    if(millis()-interval > 10000){
      Serial.println("Next Speed");
      if (cur_speed > 2) cur_speed = 1;
      else cur_speed++;
      fan_set_speed(&fan, cur_speed);
      update_status();
      interval = millis();
    }
  }
  Serial.println("Exit Rhythm mode");
  update_status();
  return;
}
void fan_sleep_mode(void){
  uint8_t cur_speed = fan.fan_speed;
  Serial.println("Enter Sleep mode");
  fan_set_speed(&fan, cur_speed);
  update_status();
  uint32_t interval = millis();
  while(fan.mode == SLEEP_MODE){
    processTimerEvents();
    if(millis()-interval > SLEEP_MODE_DURATION && cur_speed > 1){
      Serial.println("Giam toc do");
      cur_speed--;
      fan_set_speed(&fan, cur_speed);
      update_status();
      interval = millis();
    }
  }
  Serial.println("Exit Sleep mode");
  update_status();
  return;
}
void smart_config_start(void){
  if(!in_smartconfig){
    Serial.println("Enter smartconfig");
    in_smartconfig = true;
    WiFi.beginSmartConfig();
    startTimer(100,smart_config_task, NULL);
  }
}
void smart_config_stop(void){
  in_smartconfig = false;
  cancelTimer(smart_config_task);
  digitalWrite(LED_PIN,HIGH);
  WiFi.stopSmartConfig();
}
void smart_config_task(void *param){
  digitalWrite(LED_PIN, digitalRead(LED_PIN) ^ 0x01);
  if (WiFi.status() == WL_CONNECTED && WiFi.smartConfigDone()) {
    Serial.println("Exit Smart config");
    device.ssid = WiFi.SSID();
    Serial.println("Wifi Connected to [" + WiFi.SSID()+"]");
    Serial.println("Local IP:" + WiFi.localIP().toString());
    smart_config_stop();
    return;
  }
  else startTimer(100,smart_config_task,NULL);
}